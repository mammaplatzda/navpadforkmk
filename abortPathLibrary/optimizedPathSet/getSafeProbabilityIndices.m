function [ probability ] = getSafeProbabilityIndices(ind, occMap)
%GETSAFEPROBABILITYINDICES Summary of this function goes here
%   Detailed explanation goes here
%probability = prod(1-occMap.data(ind));
probability = (1-occMap.data(1))^(size(ind,1));
end

