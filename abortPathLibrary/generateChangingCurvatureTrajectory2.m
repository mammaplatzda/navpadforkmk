function [X,Y,Z,Heading,Roll,time] = generateChangingCurvatureTrajectory2(vX,vZ,roll,rollMaxAngle,rollRate,decceleration,deccZ,vMin,reactionTime,obstacleSize)
%PLOTCHANGINGCURVATURETRAJECTORY Summary of this function goes here
%   Detailed explanation goes here
numPoints = 1000;
g = 9.80665;
v = vX;
a = decceleration;
az = deccZ;

X = zeros(numPoints+1,1);
Y = zeros(numPoints+1,1);
Z = zeros(numPoints+1,1);
Heading = zeros(numPoints+1,1);
Roll = zeros(numPoints+1,1);

x = 0;
y = 0;
theta = 0;
z = 0;
if az==0
    T = (vX/a) + reactionTime;
else
    T = max([vX/a,vZ/az]) + reactionTime;
end
resolution = T/numPoints;
time = [0:resolution:T]';

zStopTime = vZ/az;
zAccTime = (T - zStopTime)/2;

rollRate = sign(rollMaxAngle)*rollRate;
for i=1:size(time,1)
    X(i) = x;
    Y(i) = y;
    Z(i) = z;
    Heading(i) = theta;
    Roll(i) = roll;    
    
    if(v >= vMin && time(i)> reactionTime)
        if(abs(y)<obstacleSize)
            roll = roll + rollRate*resolution;            
        else
            if(abs(roll)>0)
                if(abs(roll)<=abs(rollRate*resolution))
                    roll = 0;                    
                else
                    roll = roll - rollRate*resolution;                    
                end
            end
        end
        v = vX - a*(time(i)-reactionTime);
        if abs(roll) > abs(rollMaxAngle)
            roll = rollMaxAngle;
        end
        if v < 0
            v  = 0;
        end
    end

    if roll ~= 0 && v ~=0
        R = (v.*v)/(g*tan(roll));
        w = v/R;
    else
        R = 0;
        w  = 0;
    end    
    theta = theta + w*resolution;
    
    
    x = x+ (v*resolution)*cos(theta);
    y = y+ (v*resolution)*sin(theta);
    
    %---------z is totally independent of the other 2 axis ---------------%
    z =z+vZ*resolution;
    
    if time(i)<=zAccTime
        vZ = vZ + az*resolution;
    else
        vZ = vZ - az*resolution;
    end
end


