function [handleSet] = displayMapVelocityPaths(pose,map,queryState,stateMatrix,globalPathList,globalPathId,minState,stateResolution,minMapX,minMapY,mapResolution,handle)
%DISPLAYMAPVELOCITYPATHS Summary of this function goes here
%   Detailed explanation goes here
    if ~isempty(map)
        imshow(map');
    end
    hold on
    vId = value2Id(queryState,minState,stateResolution);
    vPaths = stateMatrix(vId(1),vId(2),vId(3)).pathIds;
    handleSet = zeros(size(vPaths));
    for i=1:size(vPaths,1)
        ids =  globalPathId(vPaths(i),:);
        pathxy = globalPathList(ids(1):ids(2),2:3);
        pathxy = transformPoint2Global(pathxy,pose(3),pose(1:2));
        pathxy = coord2indexNotRound(pathxy(:,1),pathxy(:,2),mapResolution,minMapX,minMapY);
        handleSet(i) = plot(pathxy(:,1),pathxy(:,2),'r');
    end
    
end


