function feature_vector = get_features( traj, robo_map, grid_params )
%GET_FEATURES Summary of this function goes here
%   Detailed explanation goes here

feature_vector = [];

unknown = zeros(size(robo_map.p_));
unknown( abs(robo_map.p_ - grid_params.p_occ_default) < 0.05 ) = 1; 
[unknown_dist, unknown_list] = bwdist( unknown );

occ = zeros(size(robo_map.p_));
occ( robo_map.p_ < 0.1 ) = 1; 
[occ_dist, occ_list] = bwdist( occ );

id = [max(1,min(round(traj.x), size(robo_map.data,1))); max(1,min(round(traj.y), size(robo_map.data,2)))];
linid = sub2ind(size(occ_dist), id(1,:), id(2,:));
feature_vector = [feature_vector; ones(size(linid))]; % Always have 1
feature_vector = [feature_vector;  occ_dist( linid )];
feature_vector = [feature_vector;  unknown_dist( linid )];

[r_occ.x, r_occ.y] = ind2sub(size(occ_list), occ_list(linid));
r_occ.x = r_occ.x - traj.x; r_occ.y = r_occ.y - traj.y;
feature_vector = [feature_vector; (r_occ.x.*cos(traj.psi) + r_occ.y.*sin(traj.psi))./sqrt(r_occ.x.^2 + r_occ.y.^2)];

[r_unk.x, r_unk.y] = ind2sub(size(unknown_list), unknown_list(linid));
r_unk.x = r_unk.x - traj.x; r_unk.y = r_unk.y - traj.y;
feature_vector = [feature_vector; (r_unk.x.*cos(traj.psi) + r_unk.y.*sin(traj.psi))./sqrt(r_unk.x.^2 + r_unk.y.^2)];

feature_vector = [feature_vector; ones(size(linid))];

end

