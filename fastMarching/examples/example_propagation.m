clc;
clear;
close all;

%% Add required paths
addpath(genpath('../toolbox_fast_marching'));
addpath(genpath('../../maps'));


%% Load a map
W = getMapfromImage('../../maps/example_map.tif');
goal = [238; 344];
figure, imagesc(W);
colormap(gray);

%% Create map and goal
% W = ones(1000,1000);
% W(350:650, 350:650) = 0;
% goal = [100; 100];
% figure, imagesc(W);
% colormap(gray);

%% Perform propagation 
options.nb_iter_max = Inf;
tic;
D = perform_fast_marching(W, goal, options);
str = sprintf('Propagation took %f s', toc);
disp(str);
figure, imagesc(D);