function display_obj = get_map_disp_obj(world_map, display_obj)
%GET_INFORMATION_DISP_OBJ Summary of this function goes here
%   Detailed explanation goes here
if (nargin<2)
    display_obj = struct();
    display_obj.handle_set = struct();
    % Figure 1: WorldMap display
    % Update: Resize window to be a quarter of the screen
    scrsz = get(groot,'ScreenSize');
    display_obj.handle_set.fig_world = figure('Position',[1 scrsz(4)/2 800 800]);
    display_obj.handle_set.world_map = imshow(world_map.data');
    axis equal;
else
    set(display_obj.handle_set.world_map,'CData', world_map.data');
end


end

